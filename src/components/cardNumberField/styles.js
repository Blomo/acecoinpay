import styled from "styled-components";

export const Container = styled.div`

  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-top: 10px;

  padding-bottom: 10px;
  margin: 0px;
  border: 1px solid #eee;
  border-radius: 5px;
`;

export const Flex = styled.div`
  width: ${({ width }) => (width ? width : "100%")};
  display: flex;
  align-items: center;
  background: ${({ background }) => (background ? background : "transparent")};
  justify-content: ${({ justifyContent }) =>
      justifyContent ? justifyContent : "flex-start"},
    ;
`;
export const Image = styled.img`
  height: 30px;
  margin-right: 40px;
`;

export const Input = styled.input`
  border: none;
  width: 4em;
  height: 40px;
  margin-left: 40px;

  &:focus {
    outline: none;
  }

  ,
  ::placeholder {
    font-size: 20px;
    color: #aaaaaa;
  }

  @media only screen and (max-width: 800px) {
    border: none;
    width: 1.2em;
    margin-left: 0px;
  }

  @media (max-width: 1200px) {
    width: ${({ resWidth }) => (resWidth ? resWidth : "100%")};
    flex-direction: ${({ resFlexDirection }) =>
      resFlexDirection ? resFlexDirection : "column"};
    justify-content: ${({ resjustifyContent }) =>
      resjustifyContent ? resjustifyContent : "space-between"};
    align-items: ${({ resalignItems }) =>
      resalignItems ? resalignItems : "center"};
    ${(props) =>
      !props.disablePadd &&
      `padding: ${({ paddingVertical }) =>
        paddingVertical ? paddingVertical : "0rem"} ${({ boxedLayout }) =>
        boxedLayout === true ? "5%" : boxedLayout === "remove" ? "" : "0"}`};
    flex-wrap: ${({ flexWrap }) => (flexWrap ? flexWrap : "wrap")};
  }
`;
