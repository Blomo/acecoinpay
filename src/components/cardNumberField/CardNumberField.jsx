import React from "react";
import { Container, Flex, Image, Input } from "./styles";
import { AiFillCheckCircle } from "react-icons/ai";
import Logo from "../../assets/logo.png";

export const CardNumberField = () => {
  return (
    <Container class="flex-between">
      <Flex justifyContent="space-between" width="100%">
        <Flex justifyContent="space-between">
          <Image src={Logo} alt="logo" />
          <Input maxLength={4} placeholder="0000" />-
          <Input maxLength={4} placeholder="0000" />-
          <Input maxLength={4} placeholder="0000" />-
          <Input maxLength={4} placeholder="0000" />
        </Flex>
        <Flex width="10%" justifyContent="center">
          <AiFillCheckCircle color="#2962ff" size="30px" />
        </Flex>
      </Flex>
    </Container>
  );
};
