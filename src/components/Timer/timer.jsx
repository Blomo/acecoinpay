import React, { useState, useEffect } from "react";
import { Time, TimerContainer } from "../../screens/styles";

export const Timer = () => {
  const [time, setTime] = useState({ minutes: 10, seconds: 0 });

  useEffect(() => {
    let timer = null;

    if (time.minutes === 60 && time.seconds === 0) {
      return;
    }

    if (time.seconds > 0) {
      timer = setTimeout(() => {
        setTime((prevTime) => ({ ...prevTime, seconds: prevTime.seconds - 1 }));
      }, 1000);
    } else if (time.minutes > 0) {
      timer = setTimeout(() => {
        setTime((prevTime) => ({ minutes: prevTime.minutes - 1, seconds: 59 }));
      }, 1000);
    }

    return () => clearTimeout(timer);
  }, [time]);

  const formatTime = (value) => {
    return value.toString().padStart(2, "0");
  };

  const renderHour = () => {
    if (time.minutes >= 60) {
      return (
        <div className="hour">{formatTime(Math.floor(time.minutes / 60))}</div>
      );
    }
    return null;
  };

  return (
    <TimerContainer className="timer">
      {renderHour()}
      <Time className="minute">{formatTime(time.minutes % 60)}</Time>

      <Time className="minute">{formatTime(time.seconds)}</Time>
    </TimerContainer>
  );
};
