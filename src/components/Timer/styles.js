import styled from "styled-components";
export const Container = styled.div`
  display: inline-block;
  margin-top: 25px;
  padding: 10px;
  display: flex;
  text-align: center;
  width: 400px;
  border-right: solid 1px rgba(255, 255, 255, 0.2);
  font-weight: 300;
  padding: 10px;
`;
