import styled from "styled-components";
import { FlexibleDiv } from "../flexibleDiv/flexibleDiv.component";

export const ContainerInput = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  width: ${({ width }) => (width ? width : "100%")};

  border: 1px solid #eee;
  border-radius: 5px;
  height: 62px;

  padding-right: ${({ paddingRight }) =>
    paddingRight ? paddingRight : "10px"};
`;

export const Flex = styled(FlexibleDiv)`
  align-items: center;
  justify-content: ${({ justifyContent }) =>
    justifyContent ? justifyContent : "space-between"};
`;

export const Input = styled.input`
  border: none;
  width: 2em;

  &:focus {
    outline: none;
  }

  ,
  ::placeholder {
    font-size: 20px;
    color: #000000;
    font-weight: 600;
  }

  @media only screen and (max-width: 600px) {
    margin-left: 0px;
  }
`;
export const RightIcon = styled.div`
  width: 50px;
  border-radius: 50px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const LeftIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
