import React from "react";
import { TbGridDots } from "react-icons/tb";
import {
  ContainerInput,
  Flex,
  Input,
  LeftIcon,
  RightIcon,
} from "./styles.customInput";
import { BoldText } from "../boldText/boldText";

export const CustomField = ({
  icon = false,
  empty = true,
  width,
  maxLength,
  placeholder,
  input1,
  paddingRight,
}) => {
  return (
    <ContainerInput width={width} paddingRight={paddingRight}>
      {input1 ? (
        <Flex resFlexDirection="row">
          <>{icon ? <RightIcon></RightIcon> : null}</>

          <Input maxLength={maxLength} placeholder={placeholder} />

          <div>
            <>
              {icon ? (
                <LeftIcon>
                  <TbGridDots size="30px" color="#aaa" />
                </LeftIcon>
              ) : null}
            </>
          </div>
        </Flex>
      ) : (
        <Flex resFlexDirection="row" justifyContent="center">
          <>{icon ? <RightIcon></RightIcon> : null}</>

          <Input maxLength={maxLength} placeholder={placeholder} />

          <div>
            <>
              {icon ? (
                <LeftIcon>
                  <TbGridDots size="30px" color="#aaa" />
                </LeftIcon>
              ) : null}
            </>
          </div>
        </Flex>
      )}
    </ContainerInput>
  );
};
