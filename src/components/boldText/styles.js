import styled from "styled-components";

export const Text = styled.p`
  font-weight: ${({ fontWeight }) => (fontWeight ? fontWeight : "600")};
  font-size: ${({ fontSize }) => (fontSize ? fontSize : "20px")};
  color: #000;
  margin: 0px;
`;
