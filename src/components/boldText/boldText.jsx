import React from "react";
import { Text } from "./styles";
export const BoldText = ({ fontSize, fontWeight, children }) => {
  return (
    <Text fontWeight={fontWeight} fontSize={fontSize}>
      {children}
    </Text>
  );
};
