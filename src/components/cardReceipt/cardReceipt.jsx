import React from "react";
import { HiWifi } from "react-icons/hi";
import {CiReceipt} from "react-icons/ci"
import Chip from "../../assets/chip.png";
import Logo from "../../assets/logo.png";
import MasterCard from "../../assets/mastercard.png";
import {
  Blue,
  Card,
  CardNumber,
  CardNumberContainer,
  Container,
  Date,
  DateContainer,
  Divider,
  Flex,
  Header,
  Left,
  Name,
  ReceiptContainer,
  Right,
} from "./styles";
import { FlexibleDiv } from "../flexibleDiv/flexibleDiv.component";
import { Text } from "../text/text";
import { BoldText } from "../boldText/boldText";

export const Receipt = () => {
  return (
    <Container>
      <ReceiptContainer>
        <Blue />
        <Card>
          <Header resFlexDirection="row">
            <img src={Chip} alt="" height={"40px"} />
            <HiWifi color="#aaaaaa" size="30px" />
          </Header>
          <Name color="#000">BLossom Onoriode</Name>

          <CardNumberContainer width="34%" resWidth="30%" resFlexDirection="row">
            <CardNumber>....</CardNumber>

            <CardNumber>....</CardNumber>

          </CardNumberContainer>
          <FlexibleDiv resFlexDirection="row"   marginTop="15px">
            <DateContainer resFlexDirection="row">
              <Date>9</Date>
              <Date>/</Date>
              <Date>10</Date>
            </DateContainer>

            <img src={MasterCard} alt="card" height={"40px"} />
          </FlexibleDiv>
        </Card>

        <Flex resFlexDirection="row">
          <Text fontSize="18px">Company</Text>

          <BoldText fontSize={"18px"} fontWeight={"600"}>
            Apple
          </BoldText>
        </Flex>
        <Flex resFlexDirection="row">
          <Text fontSize="18px">Order Number</Text>

          <BoldText fontSize={"18px"} fontWeight={"600"}>
            1266201
          </BoldText>
        </Flex>
        <Flex resFlexDirection="row"> 
          <Text fontSize="18px">Product</Text>

          <BoldText fontSize={"18px"} fontWeight={"600"}>
            MacBook Air
          </BoldText>
        </Flex>

        <Flex resFlexDirection="row">
          <Text fontSize="18px">VAT(20%)</Text>

          <BoldText fontSize={"18px"} fontWeight={"600"}>
            $199.OO
          </BoldText>
        </Flex>

        <FlexibleDiv   alignItems="flex-end" resWidth="80%" width="80%"  resjustifyContent="space-between" resAlignItems="center" resFlexDirection={"row"} >
          <FlexibleDiv resWidth="30%"   flexDirection="column" width="auto" justifyContent="flex-start">

            <Text fontSize="12px">You have to Pay</Text>
            <BoldText fontSize={"30px"} fontWeight={"600"}>
              $199.OO
            </BoldText>
          </FlexibleDiv>

          <FlexibleDiv width="10%" resWidth="auto">
          <CiReceipt size="30px" color="#aaa"/>
          
          </FlexibleDiv>
        </FlexibleDiv>
          <Divider>
            <Left></Left>
            <Right></Right>
          </Divider>
      </ReceiptContainer>
    </Container>
  );
};
