import styled from "styled-components";
import { FlexibleDiv } from "../flexibleDiv/flexibleDiv.component";
import { Text } from "../text/text";
import { BoldText } from "../boldText/boldText";

export const Container = styled.div`
  width: 90%;
  position: absolute;
  justify-content: center;
  align-items: center;


  @media (max-width: 1000px) {
    width:100%

  }
`;

export const Blue = styled.div`
  display: block;
  height: 2.5em;
  width: 5em;
  background-color: #2962ff;
  top: -80px;
  overflow: hidden;
  left: 40%;
  position: absolute;
  z-index: 1px;
`;
export const Card = styled.div`
  position: relative;

  backdrop-filter: blur(15px);
  margin: -4em 1em 1.5em 1em;
  padding: 1.5em 1.5em;
  background: linear-gradient(180deg, rgba(255, 255, 255, 0.6), #fff);
  box-shadow: 0 0.5em 1em 0.125em rgba(0, 0, 0, 0.1);
  width: 70%;

  border-radius: 10px;
`;

export const Header = styled(FlexibleDiv)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 50px;


`;
export const Name = styled(Text)`
  color: red;
`;

export const CardNumberContainer = styled(FlexibleDiv)``;

export const CardNumber = styled.p`
  font-weight: bold;
  margin: 0px;
  color: #aaa;
  font-size: 24px;
  margin-left: 10px;

  
`;

export const DateContainer = styled(FlexibleDiv)`
  padding: 10px;
  width: auto;
`;

export const Date = styled(BoldText)``;

export const ReceiptContainer = styled.div`
  background-color: #f0f3f8;
  width: 100%;
  align-items: center;
  display: flex;
  flex-direction: column;
  border-radius: 1em;
  padding-bottom:45px;
  position: absolute;
  

  @media only screen and (max-width: 800px) {
    height: auto;
  }
`;
export const Flex = styled(FlexibleDiv)`
  padding-left: 20px;
  padding-right: 10px;
  width: 80%;
`;

export const Divider = styled.div`
  bottom:6.9em;
  position: absolute;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px dashed #aaaaaa;
`;

export const Left = styled.div`
  position: absolute;
  height: 2.65em;
  width: 2.65em;
  border-radius: 50%;
  background-color: #fff;
  left: -20px;
`;
export const Right = styled.div`
  position: absolute;
  height: 2.56em;
  width: 2.56em;
  border-radius: 50%;
  background-color: #fff;
  right: -20px;
`;
