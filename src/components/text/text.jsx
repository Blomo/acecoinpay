import React from "react";
import { Text_ } from "./styles";
export const Text = ({ fontSize, color, children, fontWeight }) => {
  return (
    <Text_ color={color} fontSize={fontSize} fontWeight={fontWeight}>
      {children}
    </Text_>
  );
};
