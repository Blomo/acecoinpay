import styled from "styled-components";

export const Text_ = styled.p`
  font-size: ${({ fontSize }) => (fontSize ? fontSize : "16px")};
  color: ${({ color }) => (color ? color : "#aaaaaa")};
  font-weight: ${({ fontWeight }) => (fontWeight ? fontWeight : "400")};
`;
