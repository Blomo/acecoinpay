import React from "react";
import {
  Container,
  Flex,
  FlexDiv,
  LogoContainer,
  LogoIllustation,
  LogoName,
  Number,
  Section1,
  Section2,
  Width,
  Wrapper,
  WrapperContainer,
} from "./styles";
import { GiSwipeCard } from "react-icons/gi";
import { BoldText } from "../components/boldText/boldText";
import { Text } from "../components/text/text";
import { FlexibleDiv } from "../components/flexibleDiv/flexibleDiv.component";
import { CardNumberField } from "../components/cardNumberField/CardNumberField";
import { CustomField } from "../components/customInput/customInput";
import { Button } from "../components/button/button.component";
import { Receipt } from "../components/cardReceipt/cardReceipt";
import { Timer } from "../components/Timer/timer";
import { AiOutlineClose } from "react-icons/ai";

const AceCoinPay = () => {
  return (
    <Container>
      <FlexibleDiv justifyContent="center">

      <FlexibleDiv justifyContent="flex-end"  paddingVertical="30px" width="90%" resWidth="80%" resjustifyContent="flex-end" resalignItems="flex-end">
        <AiOutlineClose size="30" color="#aaa" />
    
      </FlexibleDiv>
      <WrapperContainer>
        <Section1>
          <Width>
            <Wrapper>
              <Flex resFlexDirection="row">
                <LogoContainer>
                  <LogoIllustation>
                    <GiSwipeCard size={24} color="white" />
                  </LogoIllustation>

                  <LogoName>AceCoin</LogoName>

                  <LogoName fontWeight>Pay</LogoName>
                </LogoContainer>

                <Timer />
              </Flex>
              <FlexibleDiv
                width="100%"
                flexDirection="row"
                resWidth="100"
                resFlexDirection="row"
              >
                <FlexibleDiv
                  flexDirection="column"
                  justifyContent="flex-start"
                  resalignItems="flex-start"
                  resjustifyContent="flex-start"
                  resWidth="70%"
                  width="50%"
                  alignItems="flex-start"
                >
                  <BoldText>Card Number</BoldText>
                  <Text>Enter the 16 digit card number on the card</Text>
                </FlexibleDiv>
                <Text color="#2962ff" fontWeight="500">
                  Edit
                </Text>
              </FlexibleDiv>

              <CardNumberField />
            </Wrapper>
            <Wrapper>
              <FlexibleDiv
                flexDirection="row"
                resalignItems="flex-start"
                resjustifyContent="flex-start"
              >
                <FlexibleDiv
                  flexDirection="column"
                  resalignItems="flex-start"
                  resjustifyContent="flex-start"
                  resWidth="70%"
                  justifyContent="flex-start"
                  width="39%"
                  alignItems="flex-start"
                >
                  <BoldText>CVV Number</BoldText>
                  <Text>Enter the 3 or 4 digit number on the card</Text>
                </FlexibleDiv>
                <FlexibleDiv width="60%">
                  <CustomField icon maxLength="2" input1 placeholder="012" />
                </FlexibleDiv>
              </FlexibleDiv>
            </Wrapper>
            <Wrapper>
              <FlexibleDiv
                flexDirection="row"
                resalignItems="flex-start"
                resjustifyContent="flex-start"
                >
                <FlexibleDiv
                  flexDirection="column"
                  justifyContent=""
                  width="35%"
                  alignItems="flex-start"
                  resalignItems="flex-start"
                  resjustifyContent="flex-start"
                  resWidth="70%"
                  >
                  <BoldText>Expiry Date</BoldText>
                  <Text>Enter the expiration date on the card </Text>
                </FlexibleDiv>
                <FlexibleDiv
                  reswidth="100%"
                  resFlexDirection="row"
                  resalignItems="center"
                  resjustifyContent="space-between"
                  width="60%"
                  >
                  <CustomField maxLength="2" placeholder="09" width="40%" />

                  <BoldText>/</BoldText>

                  <CustomField maxLength="2" placeholder="22" width="40%" />
                </FlexibleDiv>
              </FlexibleDiv>
            </Wrapper>

            <Wrapper>
              <FlexibleDiv flexDirection="row">
                <FlexibleDiv
                  flexDirection="row"
                  resalignItems="flex-start"
                  resjustifyContent="flex-start"
                  >
                  <FlexibleDiv
                    flexDirection="column"
                    resalignItems="flex-start"
                    resjustifyContent="flex-start"
                    resWidth="70%"
                    justifyContent="flex-start"
                    width="39%"
                    alignItems="flex-start"
                    >
                    <BoldText>Password</BoldText>
                    <Text>Enter your Dynamic password </Text>
                  </FlexibleDiv>
                  <FlexibleDiv width="60%">
                    <CustomField maxLength="2" input1 width="100%" icon />
                  </FlexibleDiv>
                </FlexibleDiv>
              </FlexibleDiv>
            </Wrapper>
            <Wrapper>
              <FlexibleDiv>
                <Button>Pay Now</Button>
              </FlexibleDiv>
            </Wrapper>
          </Width>
        </Section1>
        <Section2>
          <Receipt />
        </Section2>
      </WrapperContainer>
                    </FlexibleDiv>
    </Container>
  );
};

export default AceCoinPay;
