import styled from "styled-components";
import { FlexibleDiv } from "../components/flexibleDiv/flexibleDiv.component";

export const Container = styled.div`
  background-color: #fff;
  width: 90%;
  align-items:center;
  display: flex;
  flex-direction:column;
  flex-wrap: "wrap";

  @media only screen and (max-width: 1200px) {
    height: 100vh;
    width: 100%;
    justify-items: center;
    align-items: center;
    display: flex;
    flex-direction: column;
  }
  @media only screen and (max-width: 1400px) {
    height: 100vh;
    width: 100%;
    justify-items: center;
    align-items: center;
    display: flex;
    flex-direction: column;
  }
`;

export const WrapperContainer = styled.div`
  background-color: #fff;
  width: 100%;
  justify-items: space-between;
  display: flex;
  flex-wrap: "wrap";

  @media only screen and (max-width: 1200px) {
    height: 100vh;
    width: 100%;
    justify-items: center;
    align-items: center;
    display: flex;
    flex-direction: column;
  }
  @media only screen and (max-width: 1400px) {
    height: 100vh;
    width: 100%;
    justify-items: center;
    align-items: center;
    display: flex;
    flex-direction: column;
  }
`;

export const Width = styled.div`
  width: ${({ width }) => (width ? width : "90%")};

  @media only screen and (max-width: 1400px) {
    width: 100%;
  }
`;

export const TimerContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;
export const Time = styled.div`
  margin-left: 10px;
  padding: 15px;
  border-radius: 5px;
  background-color: #3a4466;
`;

export const Flex = styled(FlexibleDiv)`
  padding-bottom: 10px;
  width: 100%;
`;

export const Wrapper = styled.div`
margin-bottom :40px;
height:auto;


@media only screen and (max-width:1200px) {
padding:0px;

margin:0px;

},
`;

export const Section1 = styled.div`
  width: 65%;
  height: auto;
  display: flex;
  align-items: center;
  justify-content: center;

  @media only screen and (max-width: 800px) {
    width: 90%;
    padding-left: 0px;
    padding-right: 0px;
  }

  @media only screen and (max-width: 1200px) {
    width: 80%;
  }
  @media only screen and (max-width: 1400px) {
    width: 80%;
    padding-right: 0px;
  }
`;

export const LogoContainer = styled.div`
  background-color: #fff;
  justify-content: center;
  align-items: center;
  display: flex;

  @media only screen and (max-width: 800px) {
  }
`;

export const LogoIllustation = styled.div`
  height: 50px;
  width: 50px;
  background-color: #0057ff;
  border-radius: 50%;
  justify-content: center;
  align-items: center;
  display: flex;

  @media only screen and (max-width: 800px) {
  }
`;

export const LogoName = styled.p`
  font-weight: ${({ fontWeight }) => (fontWeight ? fontWeight : "600")};
  font-size: 24px;
  color: #000;
`;

export const CardNumberContainer = styled.div`
  background-color: white;
  width: 100%;
  border: 0.4px solid;
  padding-top: 10px;

  padding-bottom: 10px;
  border-radius: 15px;
  display: flex;

  @media only screen and (max-width: 800px) {
  }

  @media only screen and (max-width: 1000px) {
  }
`;
export const Number = styled.p`
  font-size: 18px;

  @media only screen and (max-width: 800px) {
  }
`;

export const Section2 = styled.div`
  width: 35%;
  flex-direction: column;
  padding-top: 90px;
  display: flex;
  align-items: center;
  position: relative;

  @media only screen and (max-width: 800px) {
    
    width: 100%;
  }
  @media only screen and (max-width: 1200px) {
    width: 80%;
  }
`;
