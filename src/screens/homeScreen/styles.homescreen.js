import styled from "styled-components";

export const ContainerWrapper = styled.div`
  background-image: url(${(props) => props.imgUrl});
  width: 100%;
  height: 100vh;
  justify-content: center;
  align-items: center;
  display: flex;
  background-size: cover;
  background-repeat: no-repeat;
`;
