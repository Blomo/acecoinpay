import React from "react";
import { ContainerWrapper } from "./styles.homescreen";
import Background from "../../assets/background.jpg"
import AceCoinPay from "../index";

export const HomeScreen = () => {
    return (
        <ContainerWrapper imgUrl={Background}>
            <AceCoinPay />
        </ContainerWrapper>
    )
}
